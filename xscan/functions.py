''''
MIT License

Copyright (c) 2016 Davide Scaglione (IGA - Istituito di Genomica Applicata)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
'''
import sys
import operator
import os
import time
import numpy as np
import copy
import math
#import itertools
import vcf
import gc
#import re
# import ipdb
import matplotlib.pyplot as plt
import multiprocessing as mp
from argparse import RawTextHelpFormatter
import subprocess
#from scipy import stats
#from matplotlib.ticker import MultipleLocator
from matplotlib.ticker import FuncFormatter
from matplotlib.ticker import MaxNLocator
from xscan.classes import Window
from xscan.classes import RefObj
from xscan.classes import Range
#from xscan.classes import Hcphase
from xscan.classes import RefList
import logging

#logging.basicConfig()
logger = logging.getLogger("Xscan")


def not_sig_whole_AF(ref_list):
    s1AF_list = []
    s2AF_list = []
    for refobj in ref_list:
        for window in refobj.windows:
            if not window.flag:
                s1AF_list.append(window.s1AFmed)
                s2AF_list.append(window.s2AFmed)
    
    s1AF_base = np.median(s1AF_list)
    s2AF_base = np.median(s2AF_list)
    
    return (s1AF_base, s2AF_base)




def inspect_two_cov(vcf_file, sample1, sample2):
    vcf_reader = vcf.Reader(open(vcf_file, 'r'))
    start_time = time.time()
    logger.info("Sampling coverage... ")
    counter_all = 0
    counter_good = 0
    cov1 = []
    cov2 = []
    interleave = 50
    for record in vcf_reader:
        
        call1 = record.genotype(sample1)
        call2 = record.genotype(sample2)
        if not call1.called or not call2.called:
            continue
        
        if (counter_all % interleave) == 0:
            
            cov1.append(call1.data[1][0] + call1.data[1][1])
            cov2.append(call2.data[1][0] + call2.data[1][1])
            counter_good += 1
            counter_all += 1
        else:
            counter_all += 1
            continue
            
        if counter_good == 1000:
            break

    med_cov1 = np.median(cov1)
    med_cov2 = np.median(cov2)
    end_time = time.time()
    elapsed = end_time - start_time
    logger.info("%.1f sec" % elapsed)
    logger.info("Sampled " + str(counter_good) + " SNP positions for coverage " + \
                "estimation, interleaved every " + str(interleave) + " SNPs" )
    return (med_cov1, med_cov2)

def launch_a_command(command):
    
    try:
        subprocess.check_output(command, shell=True,
                                stderr=subprocess.STDOUT)
        ## lo stesso stderr lo redireziono in stdout che verra' memorizzato
    except subprocess.CalledProcessError as e:
        # oggetto che mi tiene dati dopo l'errore
        logger.error("Unable to execute: " + command)
        logger.error(e.output)
        sys.exit()
    
    
def tabix_me(vcf_file):
    if os.path.exists(vcf_file + ".gz") and os.path.exists(vcf_file + ".gz.tbi"):
        pass
    else:
#        try:
        command1 = "bgzip -c " + vcf_file + " > " + vcf_file + ".gz"
        command2 = "tabix -p vcf " + vcf_file + ".gz"
        subprocess.check_output(command1, shell=True, stderr=subprocess.STDOUT)
        subprocess.check_output(command2, shell=True, stderr=subprocess.STDOUT)
#         except subprocess.CalledProcessError as errore:
#             logger.error("Unable to execute a tabix command: make sure tabix and " + \
#                          "its bgzip command in your path")
        
        
def define_allowed_ref(vcf_file, **kwargs):
    user_required = kwargs.get('user_required', [])
    min_lenght = kwargs.get('min_length', 1000000)
    vcf_reader = vcf.Reader(open(vcf_file, 'r'))
    #ipdb.set_trace()
    if len(user_required) > 0:
        return user_required
    else:
        for contig in vcf_reader.contigs:
            if vcf_reader.contigs[contig].length >= min_lenght: 
                user_required.append(contig)
        return user_required
        

def scan_vcf(vcf_file, sample1, sample2, SNPwin, overlap,
             out_folder, allowed_ref, **kwargs):
    
    
    #vcf_reader = vcf.Reader(open(vcf_file, 'r'))
    
    min_qual = kwargs.get('min_qual', 50)
    lower_rprs = kwargs.get('lower_rprs', -3.0)
    upper_rprs = kwargs.get('upper_rprs', 3.0)
    max_sb = kwargs.get('max_sb', 0.0)
    max_dels = kwargs.get('max_dels', 0.9)
    min_cov1 = kwargs.get('min_cov1', 15)
    max_cov1 = kwargs.get('max_cov1', 1000)
    min_cov2 = kwargs.get('min_cov2', 15)
    max_cov2 = kwargs.get('max_cov2', 1000)
    min_one_het = kwargs.get('min_one_het', 0.30)
    dict_of_set = kwargs.get('dict_of_set', False)
    using_phase_info = kwargs.get('using_phase_info', False)
    prev_chrom = None
    ref_list = RefList()
    counter = 0
    phased = 0
    unphased = 0
    #buffer_window = Window()
    
    
    ####  positions dumping routine   ####
    sample1_base = sample1.split('\/')[-1]
    sample2_base = sample2.split('\/')[-1] 
    dbsnp_handle = open(out_folder + "/" + sample1_base + "-" + sample2_base + "_dbSNPs.txt", 'w')    

    
    logger.info("Reading VCF for " + sample1 + ' VS ' + sample2 + '...')
    
    myreader = vcf.Reader(filename=vcf_file, compressed=True)
    
    for reference in allowed_ref:
        start_time = time.time()
        my_iterator = myreader.fetch(reference, 0, myreader.contigs[reference].length)
        curr_refobj = RefObj(reference)
        ref_list.append(curr_refobj)
        buffer_window = Window()
        counter = 0
        for record in my_iterator:

            if (len(record.alleles) != 2 or
                record.QUAL <= min_qual or
                len(record.alleles[0]) != 1 or
                len(str(record.alleles[1])) !=1):
                continue
            
            
            if not using_phase_info:  ## dont check quality info if using SNPs with phase info
                try:

                    if (record.INFO['ReadPosRankSum'] < lower_rprs or
                        record.INFO['ReadPosRankSum'] > upper_rprs or
                        #record.INFO['SB'] > max_sb or
                        record.INFO['Dels'] > max_dels):

                        continue
                except KeyError:
                    record.FILTER = "NO-INFO"
            
            # appending new ref obj to the list
            call1 = record.genotype(sample1)
            call2 = record.genotype(sample2)
            
            ## skip uncalled
            if not call1.called or not call2.called:
                continue
            
            r1 = call1.data[1][0]
            a1 = call1.data[1][1]
            r2 = call2.data[1][0]
            a2 = call2.data[1][1]

            ##skip low coverage only where phase data not present
            if (not using_phase_info and 
                not (((r1 + a1) >= min_cov1) and
                    ((r2 + a2) >= min_cov2) and
                    ((r1 + a1) <= max_cov1) and
                    ((r2 + a2) <= max_cov2))):
                continue
            ## discard everything that is not bi-allelic SNP

            
            if (not using_phase_info and 
                (1.0 * min([r1, a1]) / (r1 + a1) < min_one_het) and 
                (1.0 * min([r2, a2]) / (r2 + a2) < min_one_het)):
                continue
            
            ### if set of phased positions is provided only accumulate those ###
            
            if dict_of_set and int(record.POS) in dict_of_set[record.CHROM]:
                phased += 1
                pass 
            elif not dict_of_set:
                pass
            else:    

                unphased += 1
                continue
            
            ####################################################################

            # SNP taken, count it
            counter += 1

            buffer_window.s1a.append(a1)
            buffer_window.s1r.append(r1)
            buffer_window.s2a.append(a2)
            buffer_window.s2r.append(r2)
            buffer_window.pos.append(record.POS)
            
            line = record.CHROM + '\t'
            line += str(record.POS) + '\t'
            line += record.alleles[0] + '/'
            line += str(record.alleles[1]) + '\t'
            line += '\t+\t'
            line += record.alleles[0] + '\t'
            line += 'WinStart' + str(buffer_window.start) + '\t'
            line += str(1.0 * a1 / (a1 + r1)) + '\t'
            line += str(1.0 * a2 / (a2 + r2)) + '\n'
            dbsnp_handle.write(line)
            ref_list.snps_dict[str(record.CHROM) + ":" + str(record.POS)] = 1
            
            if (counter == SNPwin):

                ref_list[-1].windows.append(buffer_window)

                counter = counter - (SNPwin - overlap)
                if overlap > 0:
                    buffer_window = copy.deepcopy(buffer_window) ## need to copy to generate a new object and not modify the one in list
                    buffer_window.go_back(overlap)
                else:
                    buffer_window = Window()

            

        elapsed = time.time() - start_time
        logger.info("Done reading %s windows for %s - %.1f sec" % (len(ref_list[-1].windows),
                                                                        ref_list[-1].chrom,
                                                                        elapsed))
        
        
    if dict_of_set:
        logger.info("SNP positions with phase information: " + str (phased))
        logger.info("SNP positions w/o phase information: " + str (unphased))
        
        
    dbsnp_handle.close()    
    ref_list.mapme()  ## call the function to generate a dict of chr name and list positions
    return ref_list
    #ipdb.set_trace()

#############################  core launcher ##########################################


def run_test(obj, method, other_param):
    '''

    ## temporary disabled permutations
    if (obj.has_phase_blocks and 
        not (method == 'meta_fisher' or 
             method == 'wmw_try' or
             method == 'try_reads' or 
             method == 'phased_fisher')):   # if the permutation flag is on and method does not conflict with  permutations
        obj.permute(method, other_param)       # invoke permutation function
    else:    
    '''
    
    
    
    
    test = getattr(obj, method)
    test(**other_param)
    return obj


def func_star(a_b):
    return run_test(*a_b)
    #explode the tuple object in the three values, which go to run_test, running the test and returning the results


def run_analysis(ref_list, method, minadjp, maxgap, minwin,
                 out_folder, sample1, sample2, SNPwin,
                 overlap, gapratio, **kwargs):
    

    
    other_params = {'min_to_permute'  : kwargs.get('min_to_permute', 20),
                    'permutations_cap': kwargs.get('permutations_cap', 1024),
                    'block_thresholds'  : kwargs.get('block_thresholds', [])}
    

    no_gap_filling = kwargs.get('no_gap_filling', True)
    range_refinement = kwargs.get('range_refinement', False)
    cpunum = kwargs.get('threads', 1)
    
    
    logger.info('Running %s analysis with %s CPUs...' % (method, str(cpunum)))
    out_base = out_folder + "/" + method
    
    for refobj in ref_list:
#         if len(refobj.windows) < 20:
#             sys.stderr.write("Skipping %s since it has less than 20 windows" % refobj.chrom)
#             continue
        
        
        logger.info("#### Running on reference sequence \"%s\" ####" % refobj.chrom)
        refobj.ranges = []  ## empty ranges list just to be sure...
        gc.collect()
        # now this loop only do serial cleaning of results and restore empty switches
        logger.debug("@@ cleaning cached results... ")
        start_time = time.time()
        
        for window in refobj.windows:
            window.clean_results()
            
        elapsed = time.time() - start_time
        logger.debug("%.1f sec to clean results" % elapsed)
        
        #cpunum = mp.cpu_count() # get them from system
#         if cpunum > 1:
#             logger.debug("@@ multi-thread - distributing pools... ")
#             start_time = time.time()
#             pool = mp.Pool(cpunum, maxtasksperchild=1000)
#             # generate a pool with that number of cpus
#             
#             refobj.windows = pool.map(func_star, itertools.izip(refobj.windows,
#                                                                 itertools.repeat(method),
#                                                                 itertools.repeat(other_params)))
#          
#             pool.terminate()
#             pool.close()
#             pool.join()
#             gc.collect()
#             end_time = time.time()
#             elapsed = end_time - start_time
#             logger.info("%.1f sec" % elapsed)

        #else:
        logger.debug("@@ running single thread... ")
        start_time = time.time()
        for window in refobj.windows:
            run_test(window, method, other_params)
        end_time = time.time()
        elapsed = end_time - start_time
        logger.info("%.1f sec" % elapsed)
            
        
        
        
        
        #pool.j
        # map function map the func_star function to the iterables 
        #(my windows and the method, repeated with itertools)
        # izip generates the tuple object as iterable
        # func_start return the result and pool.map manages to recollect
        # to the results to refobj.windows...mantaining the order
        
            
#     end_time = time.time()
#     elapsed = end_time - start_time
#     logger.debug("%.1f sec\n" % elapsed)
    
    #### BH correction #####
    start_time = time.time()
    logger.info("Applying Benjamini-Hockberg correction...")
    bh_correct(ref_list, method, min_adjp=minadjp)
    end_time = time.time()
    elapsed = end_time - start_time
    logger.info("%.1f sec" % elapsed)
    #########################
    ### gapfill1 - ranges - gapfill2 pipeline ###
    
    for refobj in ref_list:
        
        if not no_gap_filling:
            logger.info("Filling gaps (phase 1 - max " + str(int(maxgap)) + " gaps")
            fill_gaps(refobj, maxgap)
            
            
        logger.info("Getting ranges...")
        get_ranges(refobj, minwin, SNPwin, overlap)
        
        if not no_gap_filling:   
            logger.info("Filling gaps (phase 2 - max gap/neighborhood ratio: " + str(gapratio))
            fill_gaps2(refobj, gapratio)
            
        if range_refinement:
            logger.info("Refining ranges...")
            refine_ranges(refobj)
    #############################################
        
    sample1_base = sample1.split('\/')[-1]
    sample2_base = sample2.split('\/')[-1]
    
    
    if not os.path.exists(out_base):
        os.makedirs(out_base)
    
    logger.info("Plotting results...")
    graphic_out = out_base + "/" + sample1_base + "-" + sample2_base + ".svg"
    draw_plot(ref_list, sample1_base, sample2_base, graphic_out)


    ####  ranges dumping routine   ####
   
    ranges_handle = open(out_base + "/" + sample1_base + "-" + \
                         sample2_base + "_sig_ranges.txt", 'w')
    ranges_handle.write("\t".join(['#chr', 'start', 'end',
                                   sample1_base + "_median_AF",
                                   sample2_base + "_median_AF",
                                   sample1_base + "_REFreads",
                                   sample1_base + "_ALTreads",
                                   sample2_base + "_REFreads",
                                   sample2_base + "_ALTreads",
                                   'median_adjp',
                                   '#windows', '#SNPs']) + '\n')

    for refobj in ref_list:
        for my_range in refobj.ranges:
            line = ''
            line += refobj.chrom + '\t'
            line += str(my_range.start) + '\t'
            line += str(my_range.end) + '\t'
            line += str(my_range.s1AFmed) + '\t'
            line += str(my_range.s2AFmed) + '\t'
            line += str(my_range.cum_counts[0]) + '\t'
            line += str(my_range.cum_counts[1]) + '\t'
            line += str(my_range.cum_counts[2]) + '\t'
            line += str(my_range.cum_counts[3]) + '\t'
            line += str(my_range.adjp_med) + '\t'
            line += str(my_range.winum) + '\t'
            line += str(my_range.numSNP) + '\n'
            
            ranges_handle.write(line)
    
    
    ####  windows dumping routine   ####
    
    wins_handle = open(out_base + "/" + sample1_base + "-" + \
                         sample2_base + "_all_windows.txt", 'w')
    
    wins_handle.write("\t".join(['#chr', 'start', 'end',
                                 "pvalue", "adjp_BH", "Z-score|chi2",
                                 sample1_base + "_medianAF",
                                 sample2_base + "_medianAF",
                                 "test/filling"]) + '\n')
    
    for refobj in ref_list:
        for window in refobj.windows:
            line = ''
            line += refobj.chrom + '\t'
            line += str(window.start) + '\t'
            line += str(window.end) + '\t'
            line += str(window.pvalue) + '\t'
            line += str(window.adjp) + '\t'
            line += str(window.zscore) + '\t'
            line += str(window.s1AFmed) + '\t'
            line += str(window.s2AFmed) + '\t'
            line += str(window.test) + '\n'
            wins_handle.write(line)

    wins_handle.close()
    

def bh_correct(ref_list, method, **kwargs):
    #here provide the original data structure as list of chr object
    list_of_dict = []
    min_adjp = kwargs.get('min_adjp', 1e-3)
    #generate a list of dict to sort by pvalue genome-wide
    for chrobj in ref_list:
        #####
        for window in chrobj.windows:
            label = chrobj.chrom + "-" + str(window.start)
            dictio = {'label': label, 'pvalue': window.pvalue}
            list_of_dict.append(dictio)
    
    # sort the list by pvalue
    newlist = sorted(list_of_dict,
                     key=operator.itemgetter('pvalue'),
                     reverse=False)
    #iterate and calculate adj pval and store in faster dict
    prev_bh_value = 0
    counter = 0
    faster_dict = {}
    num_total_tests = len(newlist)
    
    for element in newlist:
        p_value = element['pvalue']
        bh_value = p_value * num_total_tests / (counter + 1)
        bh_value = min(bh_value, 1)
        bh_value = max(bh_value, prev_bh_value)
        prev_bh_value = bh_value
        faster_dict[element['label']] = bh_value
        counter += 1
    # loop window object and append adj from fast dict
    
    for chrobj in ref_list:
        for window in chrobj.windows:
            label = chrobj.chrom + "-" + str(window.start)
            adjp = faster_dict[label]
            window.adjp = adjp
            if adjp <= min_adjp:
                window.flag = True
                window.test = method
            else:
                window.test = 'negative'


def fill_gaps(refobj, allowed_gap):
    windows = refobj.windows
    flag_coord = [f for f, window in enumerate(windows) if window.flag == True]  # position of flagged windows
        
    for i in range(0, len(flag_coord) - 1):    ## loop flagged pos
        distance_next = flag_coord[i + 1] - flag_coord[i]  ## distance with next positive
        gap = distance_next - 1
        if  (distance_next > 1) and (gap <= allowed_gap):  # filling threshold
            to_fill = flag_coord[i + 1] - flag_coord[i]
            filling = []
            for j in range(1, to_fill):
                filling.append(flag_coord[i] + j)
            flag_coord[i + 1:i + 1] = filling
    for i in flag_coord:
        if not windows[i].flag:
            windows[i].test = 'Filled'
        windows[i].flag = True


def get_ranges(refobj, minwin, SNPwin, overlap):
    
    state = False
    range_list = []
    
    for window in refobj.windows:
        # generate a preliminar set of ranges desite their length
        if (window.flag == True):
            if (state == False):
                range_list.append(Range(SNPwin, overlap, minwin))  # initialize range with window/overlap info
            range_list[-1].windows.append(window)               ## list of window objects
            state = True
        else:
            state = False
            continue
    ## only take those that respect the minimum number of windows
    refobj.ranges = [m for m in range_list if m.winum >= minwin]


def fill_gaps2(refobj, gapratio):
    ranges = refobj.ranges
    try:
        SNPwin = ranges[0].SNPwin
        overlap = ranges[0].overlap
        minwin = ranges[0].minwin
    except IndexError:
        return None
        
        
    for i in range(0, len(ranges) - 1):
        size_left = ranges[i].end - ranges[i].start
        size_right = ranges[i + 1].end - ranges[i + 1].start
        gap = ranges[i + 1].start - ranges[i].end
        if gap < gapratio * min(size_left, size_right):
            lastwin = ranges[i].windows[-1]
            firstwin = ranges[i + 1].windows[0]
            leftpos = refobj.windows.index(lastwin)
            rightpos = refobj.windows.index(firstwin)
            for i in range (leftpos + 1, rightpos):
                refobj.windows[i].flag = True
                refobj.windows[i].test = 'LargeGapFill'
                
    get_ranges(refobj, minwin, SNPwin, overlap)
    
        
def refine_ranges(refobj):
    for myrange in refobj.ranges:
        median_adjp = np.median(myrange.adjp_list)
        stdev_adjp = np.std(myrange.adjp_list)
        max_walk = math.floor(myrange.SNPwin / (myrange.SNPwin - myrange.overlap)) - 1
        # constraint to max logical expected

        cut = 0
        
        for i in (0, -1):
        
            cut = 0
            while cut < max_walk: 
                eW = -np.log(myrange.windows[i].adjp) / np.log(10)
                eM = -np.log(median_adjp) / np.log(10)
                eS = -np.log(stdev_adjp) / np.log(10)
                #print myrange.windows[i].start
                #print "%s %s %s" % (eW, eM, eS)
                if (eW < eM - 3 * eS):

                    myrange.windows[i].flag = False
                    myrange.windows[i].test = '_Trimmed_'
                    to_remove = myrange.windows.index(myrange.windows[i])
                    del myrange.windows[to_remove]
                    cut += 1
                    continue
                else:
                    break
            
def draw_plot(ref_list, sample1, sample2, out_name):
    f, axarr = plt.subplots(len(ref_list), sharex=False,
                            figsize=(15, 5 * len(ref_list) + 3))
    
    if len(ref_list) == 1:
        axarr = [axarr]

    title = sample1 + " VS. " + sample2
    f.suptitle(title, fontsize=20)
    f.subplots_adjust(hspace=0.2)
    sub_counter = 0

    for chrobj in ref_list:
        s1_values = []
        s2_values = []
        x_range = []
        for window in chrobj.windows:
            s1_values.append(window.s1AFmed)
            s2_values.append(window.s2AFmed)
            x_range.append(window.center)
        draw_subplot(axarr, x_range, s1_values, s2_values, sub_counter,
                     sample1, sample2, prefix=chrobj.chrom)
        draw_highlights(axarr, sub_counter, chrobj.ranges, ref_list)
        sub_counter += 1

    plt.savefig(out_name, pad_inches=0, format='SVG')

    
def draw_highlights(axarr, sub_counter, ranges, ref_list):
    
    (s1AF_base, s2AF_base) = not_sig_whole_AF(ref_list)

    for my_range in ranges:
        s1_delta = abs(s1AF_base - my_range.s1AFmed)
        s2_delta = abs(s2AF_base - my_range.s2AFmed)
        if s1_delta > s2_delta:
            color = '#44FF00'
        else:
            color = '#FF0000'
        axarr[sub_counter].axvspan(my_range.start, my_range.end,
                                   color=color, alpha=0.3)


def draw_subplot(axarr, x_range, s1_values, s2_values,
                 sub_counter, name1, name2, **kwargs):
    formatter = FuncFormatter(millions)
    min_y = 0
    max_y = 1

    ''' Setup figure and axes '''
#     
#     if len(x_range) < 200:
#         linestyle = 'o'
#     else:
#         linestyle = None
    name = kwargs.get('prefix', '')
    axarr[sub_counter].set_ylim([min_y, max_y])
    axarr[sub_counter].set_title(name)
    axarr[sub_counter].plot(x_range, s1_values,
                    #linestyle,
                    #markersize=4,
                    color='#44FF00',
                    #axes=None,
                    linewidth=1,
                    label=name1,
                    antialiased=True)
    axarr[sub_counter].plot(x_range, s2_values,
                    #linestyle,
                    #markersize=4,
                    color='#FF0000',
                    linewidth=1,
                    #axes=None,
                    label=name2,
                    antialiased=True)
    
    axarr[sub_counter].xaxis.set_major_formatter(formatter)
    axarr[sub_counter].xaxis.set_major_locator(MaxNLocator(10))
    axarr[sub_counter].legend(loc='upper center', shadow=False)
    axarr[sub_counter].set_ylabel('AF')


def millions(x, pos):
    'The two args are the value and tick position'
    return '%1.1fM' % (x * 1e-6)   
