'''
MIT License

Copyright (c) 2016 Davide Scaglione (IGA - Istituito di Genomica Applicata)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

Created on May 26, 2014

@author: dscaglione
'''
import re
from random import shuffle
from xscan.classes import Hcphase
#from Xscan.classes import Window
import logging
#import ipdb
#logging.basicConfig()
logger = logging.getLogger("Xscan")


## first function reads the hapcut file, returns a list of HCphase objects 
def read_hapcut(hapcut_file, snps_dict):
    phase_list = []
    prev_good_pos = 0
    prev_chr = ''
    handler = open(hapcut_file, 'r')
    for line in handler.readlines():
        #print line
        if re.search(r'^BLOCK:', line):
            fields = line.split(' ')
            current_phase = Hcphase()
            #phase_list.appen
            continue
        if re.search(r'(^\*|^$)', line):
            phase_list.append(current_phase)
            continue
        
        fields = line.split('\t')
        if (fields[3] != prev_chr):
            prev_good_pos = 0
        
        if ((int(fields[4]) > prev_good_pos) and (fields[3] + ":" + fields[4] in snps_dict)):
            strings = tuple([fields[x] for x in [1, 3, 4, 5, 6]])
            current_phase.add(strings)
            prev_good_pos = int(fields[4])
            prev_chr = fields[3]
        elif (fields[3] + ":" + fields[4] not in snps_dict):
            #prev_good_pos = int(fields[4])
            logger.debug('not included as not in snps_dict:')
            logger.debug(line)
            
        elif (int(fields[4]) <= prev_good_pos): 
            # need to keep out all nested phase blocks as 
            # they mess up the indexing system for fast retrival of phase during pairing with windows
            logger.debug('not included as position is before previous one:')
            logger.debug(line)
        else:
            raise Exception(line)
        
        
    return phase_list

## second function simply filter the list of HCphase objects
def filter_phases(phase_list, **kwargs):
    
    min_phased = kwargs.get('min_phased', 5)
    filter_list = []
    for phase in phase_list:
        if phase.phased >= min_phased:
            filter_list.append(phase)
    return filter_list
    

## third function creates a dict of sets to fast lookup of phased positions
def generate_phase_set(phase_list):
    logger.info("Generating a lookup set of phased positions...")
    dict_of_set = {}
    for phase in phase_list:
        if not phase.chrom in dict_of_set.keys():
            dict_of_set[phase.chrom] = set()
        for pos in phase.pos:
            dict_of_set[phase.chrom].add(pos)
    return dict_of_set

## assign phases to each chromosome
def phases_to_ref(filter_list, ref_list):
    logger.info("Distributing phases to chromosomes...")
    if filter_list:
        for phase in filter_list:
            ref_list[ref_list.map[phase.chrom]].phases.append(phase)
    else:
        return

        
def distribute_blocks(ref_list):
## distribute blocks on windows also splitting them to rescue information for overlapping wins 
## final result is a list of list in self.blocks
    
    logger.info("Synchronyzing phases to windows...")
    for refobj in ref_list:
        if len(refobj.phases) == 0:
            logger.warning("%s did not retrieved any phase..." % refobj.chrom)
            continue
        map_phases = [i.start for i in refobj.phases]
        min_pos = 0
        for window in refobj.windows:
            
            #print window
            #print "Appending to window %s - %s" % (window.start, window.end)
            window.has_phase_blocks = True
            for s in range(min_pos, len(map_phases) - 1): # all but he last
                if (window.start >= map_phases[s] and 
                    window.start < map_phases[s + 1]): # make sure window start is below the next
                    min_pos = s  # getting the index of the first phase overlapping the window
                    break
            if window.start >= map_phases[-1]: # just for the last phass, not checking the next
                min_pos = len(map_phases) - 1
                
            max_pos = None
            for s in range(min_pos, len(map_phases)):
                if (window.end < map_phases[s]): # hitting the first phase starting after window
                    max_pos = s - 1 # so get the previous one
                    break
            if max_pos is None: # if tnever hit so it's the last
                max_pos = len(map_phases) - 1
                
            #window.blocks = [min_pos, max_pos]

            fetched = refobj.phases[min_pos:max_pos + 1]
            #print len(window.blocks)
            pos = 0
            #mybuffer = []
            for phase in fetched:
#                 if 18617541 in window.pos:
#                     print "Got a fishy window with 18617541"
#                     ipdb.set_trace()
                
                start = phase.pos.index(window.pos[pos])
                try:
                    end = phase.pos.index(window.end)
                except ValueError:
                    end = len(phase.pos) - 1
                    
#                 if 18617541 in window.pos:
#                     print "Got a fishy window with 18617541"
#                     ipdb.set_trace()
                #print phase.pos[start:end + 1]
                #print hex(id(window.blocks))
                #print hex(id(window.pos))
                     
                window.blocks.append(phase.pattern[start:end + 1])
                pos = sum([len(i) for i in window.blocks])
                #print "Accumulated %s positions" % str(pos)
                #if end < len(window.pos):
                #pos = window.pos.index(window.pos[end] + 1)
                #len(window.blocks)


def blind_blocks(ref_list, param):
    logger.info("Generating blind blocks...")
    for refobj in ref_list:
        for window in refobj.windows:
            blocks = []
            abuffer = [window.pos[0]]
            assert window.has_phase_blocks == False
            
            for pos in window.pos[1:]:
                if ((pos - abuffer[0] <= param[1]) and (len(abuffer) < param[0])):
                    abuffer.append(pos)
                else:
                    blocks.append(abuffer)
                    abuffer = [pos]
                    
            blocks.append(abuffer)
            
            #print blocks
            # simply convert all to 1 
            for block in blocks:
                block = [1 for pos in block if pos > 0]
            window.blocks = blocks
            #print blocks


