#!/usr/bin/env python
'''
Copyright (c) 2016 Davide Scaglione (IGA - Istituito di Genomica Applicata)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

'''

import numpy as np
import math
#import ipdb
import scipy
from scipy import stats
import copy
#from random import shuffle
#import gc
#import itertools
import logging

#logging.basicConfig()
logger = logging.getLogger("Xscan")



def af_calc(allele1, allele2):
    tot = allele1 + allele2
    ratio = float(allele1) / float(tot)
    return ratio

# def reduce_masks(masks, min_to_permute, permutation_cap):
    # permutations = 1000000000
    # while permutations > permutation_cap:
        # fixed_positions = [i for i in xrange(len(self.blocks)) if len(self.blocks[i]) < min_to_permute]
        # reduced_masks = []
        # for mask in masks:
            # mask_as_list = [x for x in mask]
            # for i in fixed_positions:
                # mask_as_list[i] = '0'
            # reduced_masks.append(''.join(mask_as_list))
        # reduced_masks = list(set(reduced_masks))
        # shuffle(reduced_masks)
        # masks = reduced_masks[:]
        # permutations = 2**len(masks)
        # min_to_permute += 1
    
    
class Window(object):

    def __init__(self):
        # unmutable attributes #
        self.s1r = []
        self.s1a = []
        self.s2r = []
        self.s2a = []
        self.pos = []
        self.blocks = []
        self.has_phase_blocks = False
        # analysis dependent attributes #
        self.sample1_AF = []
        self.sample2_AF = []
        self.toswitch = []  # the mask array to calculatr the s[r/a][1/2]_op arrays for testing
        self.zscore = ''
        self.pvalue = 1
        self.adjp = 1
        self.flag = False
        self.test = ''
        self.top_tests = []
        
    def clean_results(self):
        self.sample1_AF = []
        self.sample2_AF = []
        self.toswitch = []
        self.zscore = ''
        self.pvalue = 1
        self.adjp = 1
        self.flag = False
        self.test = ''
        self.top_tests = []
        #gc.collect() cretino! ci mette un secolo a fare un gc ad ogni chiamata del metodo.
        
    def toswitch_from_mask (self, mask):
        ## function to generated toswitch array on thebase of higher level mask on blocks
        ## utility for permutation of blocks
        self.toswitch = []
        for block, switch in zip(self.blocks, mask):   # mask is a string of 0/1
            
            if int(switch) == 1:
                temp = []
                for site in block:
                    if site == 0:
                        temp.append(1)
                    else:
                        temp.append(0)    
                self.toswitch += temp
            else:
                self.toswitch += block
                #self.toswitch is a final mask with equal length to SNP positions
            #ipdb.set_trace()
        #ipdb.set_trace()
    
    
    def permute(self, method, perm_params):
        #gc.collect()
        min_to_permute = perm_params['min_to_permute']
        permutations_cap = perm_params['permutations_cap']
        #mask_lol = perm_params['mask_lol']
        orig_length = len(self.blocks)
        print "This many original blocks %s" % (orig_length)
        max_n = int(math.ceil(np.log(permutations_cap)/np.log(2)))
        fixed_positions = [i for i in xrange(len(self.blocks)) if 
                           len(self.blocks[i]) < min_to_permute]
        #ipdb.set_trace()
        permutables = orig_length - len(fixed_positions)
        #print "This many starting permutables %s" % (permutables)
        while permutables > max_n:
            print "still need to shave off"
            #ipdb.set_trace()
            min_to_permute += 1
            fixed_positions = [i for i in xrange(len(self.blocks)) if 
                               len(self.blocks[i]) < min_to_permute]
            
            permutables = orig_length - len(fixed_positions)
            print "Permutables now: %s" % (permutables)
            #ipdb.set_trace()
            
        self.toswitch = []
        masks = []
        reconstr_masks = []

        for x in range(2**permutables):
            masks.append(''.join(str((x>>i)&1) for i in xrange(permutables-1,-1,-1)))
        masks = masks[:len(masks) / 2 ]  
        #print "Masks complexity before: %s" % (str(len(masks)))
        
        for mask in masks:
            mask_as_list = [x for x in mask]
            for i in fixed_positions:
                mask_as_list[i:i] = "0"
            reconstr_masks.append(''.join(mask_as_list))
        #permutations = 1000000000
        masks = reconstr_masks[:]

        print "Calculating window @ %s with %s permutations" % (self.start, str(len(masks)))
        
        for mask in masks:

            self.toswitch_from_mask(mask)
            run_test = getattr(self, method)
            run_test()  ## RUN TMETHOD HERE
            
            
            if len(self.top_tests) < 5:
                memo = MemoTest(self)
                self.top_tests.append(memo)
            else:
                for test in self.top_tests:
                    if self.pvalue < test.pvalue: 
                        self.top_tests.sort(key=lambda x: x.pvalue)
                        self.top_tests.pop()
                        memo = MemoTest(self)
                        self.top_tests.append(memo)
                        break
        #ipdb.set_trace()
        del masks
        del reconstr_masks
        del fixed_positions
        #gc.collect()
        
        if len(self.top_tests) > 0:
            self.top_tests.sort(key=lambda x: x.pvalue)
            best_test = self.top_tests[0]
            #print  best_test.pvalue
            self.pvalue = best_test.pvalue
            self.toswitch = best_test.toswitch
            self.generate_ops()
            self.calculate_AFs()
            #print len(self.toswitch)
        #ipdb.set_trace()
        #print "Calculating window @ %s with %s permutations...DONE!" % (self.start, str(len(reduced_masks)))

                
    def generate_ops(self):
    ## ugly but more efficient than nested funtions
    ## function return the array of count based on switches mask
        if len(self.toswitch) == 0:
            self.s1r_op = self.s1r
            self.s1a_op = self.s1a
            self.s2r_op = self.s2r
            self.s2a_op = self.s2a
        else:
            self.s1r_op = []
            self.s1a_op = []
            self.s2r_op = []
            self.s2a_op = []
            
            if len(self.toswitch) != len(self.s1r):
                raise Exception ("Switch array has not the same length")
            for i in range(0, len(self.toswitch)):
                if self.toswitch[i] == 1:
                    self.s1r_op.append(self.s1a[i])
                    self.s1a_op.append(self.s1r[i])
                    self.s2r_op.append(self.s2a[i])
                    self.s2a_op.append(self.s2r[i])
                else:
                    self.s1r_op.append(self.s1r[i])
                    self.s1a_op.append(self.s1a[i])
                    self.s2r_op.append(self.s2r[i])
                    self.s2a_op.append(self.s2a[i])
#################################################################################

#### Utility properties #########################################################

    @property
    def start(self):
        return self.pos[0]
        
        
    @property
    def end(self):
        return self.pos[-1]
    
    @property
    def center(self):
        return self.pos[len(self.pos)/2]
    
    
    @property
    def s1AFmed(self):
        return np.median(self.sample1_AF)
        
        
    @property
    def s2AFmed(self):
        return np.median(self.sample2_AF)
        
        
    def go_back(self, how_much):
        self.s1r = self.s1r[-how_much:]
        self.s1a = self.s1a[-how_much:]
        self.s2r = self.s2r[-how_much:]
        self.s2a = self.s2a[-how_much:]
        self.pos = self.pos[-how_much:]
        

    def calculate_AFs(self):
        # just use _op values after switching to calculate AF
        win_len = len(self.pos)
        self.sample1_AF = []
        self.sample2_AF = []
        for i in range(0, win_len):
            s1_alt_ratio = af_calc(self.s1a_op[i], self.s1r_op[i])
            self.sample1_AF.append(s1_alt_ratio)
            s2_alt_ratio = af_calc(self.s2a_op[i], self.s2r_op[i])
            self.sample2_AF.append(s2_alt_ratio)
            del s1_alt_ratio
            del s2_alt_ratio

    def try_adjust_double(self):
        #print "Running double switch"
        self.toswitch = [] # redundant to the clean function which operates at method launching
        self.generate_ops()
        self.calculate_AFs()
        for i in range(0, len(self.pos)):
            two_AF = [self.sample1_AF[i], self.sample2_AF[i]]
            two_delta = map(lambda x: abs(x - 0.5), two_AF) # two delta values
            strongest = two_delta.index(max(two_delta))
            switch = (two_AF[strongest] > 0.5) 
            # if the ALT allele freq of the highest delta sample
            # is greater than 0.5 then it must be switched
            self.toswitch.append(switch)
        
#### Statistic test routines #################

    
    def wmw_alt(self, **kwargs):
        self.generate_ops()
        self.calculate_AFs()
        (zscore, pvalue) = stats.ranksums(self.sample1_AF, self.sample2_AF)
        self.zscore = zscore
        self.pvalue = pvalue

        
    def wmw_try(self, **kwargs):
        self.try_adjust_double() # this function takes care of generating ops
        self.wmw_alt()


    def chi_count(self, **kwargs):
        ## for phase on reference, no need of ops only standard calculation of AFs
        self.generate_ops()
        self.calculate_AFs()
        one_is_lower = 0
        two_is_lower = 0
        tie = 0
        for i in range(0, len(self.pos)):
            if self.sample1_AF[i] < self.sample2_AF[i]:
                one_is_lower += 1
            elif self.sample1_AF[i] > self.sample2_AF[i]:
                two_is_lower += 1
            else:
                tie += 1
        
        exp = (len(self.pos) - tie) / 2.0        
        chi_value, pvalue = stats.chisquare(np.array([one_is_lower, two_is_lower]),
                                                  np.array([exp ,exp]))
        
        self.zscore = chi_value
        self.pvalue = pvalue
        
    
    def phased_fisher(self, **kwargs):
        # a simple #SNP long of position to swith 
        #print kwargs.get('block_thresholds')
        
        #print self.blocks
        min_block_size = int(kwargs.get('block_thresholds')[0])
        min_independent_blocks = int(kwargs.get('block_thresholds')[1])
        min_analyzed_SNPs_perwin = int(kwargs.get('block_thresholds')[2])
        
        
        
        self.generate_ops()
        self.calculate_AFs() 
        ## want AF before the ops are shuflled by hapblocks: pursuing a better plotting
        self.toswitch = sum(self.blocks, []) #concatenate lists of a list
        
        self.generate_ops() # need to align ALT allele to provide coungruent phase and thus
                            # additive property of reads count for the fisher test 
                            #(useful if phase information is provided somehow)
        #print self.blocks
        weights = []
        #print "Analyzing %s-%s" % (self.pos[0], self.pos[-1])
        pvalue_list = []
        pointer = 0
        #print "Using min block size of %s" % min_block_size
        analyzed_blocks = 0
        #directions = []
        #print self.blocks
        for b in range(0, len(self.blocks)):
            #if len(self.blocks[b]) < 10:
            #    continue
            #print len(self.blocks[b]),min_block_size
            #print len(self.blocks[b]) < min_block_size
            if (len(self.blocks[b]) < min_block_size):
                pointer += len(self.blocks[b])
            
                # just update pointer
            else:
                ## append the weight
                weights.append(len(self.blocks[b])) 
            
            
                analyzed_blocks += 1
                block_s1r = sum(self.s1r_op[pointer:pointer + len(self.blocks[b])])
                block_s1a = sum(self.s1a_op[pointer:pointer + len(self.blocks[b])])
                block_s2r = sum(self.s2r_op[pointer:pointer + len(self.blocks[b])])
                block_s2a = sum(self.s2a_op[pointer:pointer + len(self.blocks[b])])    
                
                #print block_s1r, block_s1a, block_s2r, block_s2a
                oddsratio, pvalue = stats.fisher_exact([[block_s1r, block_s1a],
                                                        [block_s2r, block_s2a]])
                # two-tailed test!!
                #delta_s1 = abs(block_s1r - block_s1a) / float(block_s1r)
                #delta_s2 = abs(block_s2r - block_s2a) / float(block_s2r)
                
                
                # weigths= [len(block) for block in self.blocks]
                #directions.append([delta_s1, delta_s2].index(max(delta_s1,delta_s2)))
                #print pvalue
                if pvalue == 1:
                    pvalue = 0.99
                
                pvalue_list.append(pvalue)
                pointer += len(self.blocks[b])
            
        
        ## the following assume a normal distribution with median=0 and stdev=1
        #print pvalue_list
        #print weights
        Zi_list = map(lambda x: stats.norm.ppf(1 - x), pvalue_list)
        
        if len(weights) < min_independent_blocks or sum(weights) < min_analyzed_SNPs_perwin:
            self.pvalue = 1
            self.zscore = 0
        assert len(weights) == len(Zi_list)
        
        
        
        #moduled_weigths = [ -x if direction == 1  else x for x, direction in zip(weights, directions)]
        
        numerator = sum([a*b for a,b in zip(weights,Zi_list)])
        #numerator = sum([a*b for a,b in zip(weights,Zi_list)])
        denominator = scipy.sqrt(sum([a**2 for a in weights]))
        Z = numerator / denominator
        self.pvalue = 1 - stats.norm.cdf(Z)
        self.zscore = Z
        #print directions
        
        '''
        numerator_s1 = sum([a*b for a,b,direction in zip(weights, Zi_list, directions) 
                            if direction == 0])
        
        denominator_s1 = scipy.sqrt(sum([a**2 for a, direction in zip(weights, directions)
                                         if direction == 0]))
        
        numerator_s2 = sum([a*b for a,b,direction in zip(weights, Zi_list, directions) 
                            if direction == 1])
        
        denominator_s2 = scipy.sqrt(sum([a**2 for a, direction in zip(weights, directions) 
                                         if direction == 1]))
        
        Z_s1 = numerator_s1 / denominator_s1
        Z_s2 = numerator_s2 / denominator_s2
        
        self.pvalue = 1 - stats.norm.cdf(abs(Z_s1 - Z_s2))
        '''
        
        #stats.norm.cdf(0.1) # equal to pnorm in R  (the probability a given number is less)
        #stats.norm.ppf(0.1) # equal to qnorm in R  (the inverse of Pnorm) provided a probability it 
        #returns the associated value in the cumulative probability distribution which is the Z-score
        
        
        
        
        '''
        lnpvalue = map(lambda x: np.log(x), pvalue_list) #np.log is ln
        chi = -2 * sum(lnpvalue)
        self.pvalue = stats.chisqprob(chi, 2 * analyzed_blocks)
        
        print self.pvalue
        self.zscore = chi
        '''

    def meta_fisher(self, **kwargs):
        self.generate_ops() # generating ops right after cleaning cycle
        self.calculate_AFs()
        pvalue_list = []
        for i in range(0, len(self.pos)):
            oddsratio, pvalue = stats.fisher_exact([[self.s1r_op[i], self.s1a_op[i]],
                                                    [self.s2r_op[i], self.s2a_op[i]]])
            
            pvalue_list.append(pvalue)
        lnpvalue = map(lambda x: np.log(x), pvalue_list) #np.log is ln
        chi = -2 * sum(lnpvalue)
        self.pvalue = stats.chisqprob(chi, 2 * len(self.pos))
        self.zscore = chi
        
        
        ### check direction of deviations #### # skip this shit by now... 
#         s1_dev = 0
#         s2_dev = 0
#         tie = 0
#         for i in range(0, len(self.pos)):
#             delta_s1 = abs(self.s1r_op[i] - self.s1a_op[i]) / float(self.s1a_op[i])
#             delta_s2 = abs(self.s2r_op[i] - self.s2a_op[i]) / float(self.s2a_op[i])
#             if delta_s1 > delta_s2:
#                 s1_dev += 1
#             elif delta_s1 < delta_s2:
#                 s2_dev += 1
#             else:
#                 tie += 1
#         exp = (len(self.pos) - tie) / 2.0
#         print self.pvalue
#         print [self.pos[0]],[s1_dev, s2_dev],[exp,exp,tie]
#         dev_chi, dev_pvalue = stats.chisquare(np.array([s1_dev, s2_dev]),
#                                                     np.array([exp ,exp]))
#         
#        
#         if (dev_pvalue < 0.05):
#             print "yeahh"
#             pass
#         else:
#             self.pvalue = 1
        #####################################
        
        
                
    def chi_reads(self, **kwargs):
        self.generate_ops()
        self.calculate_AFs()
        s1r_tot = sum([count for count in self.s1r])
        s1a_tot = sum([count for count in self.s1a])
        s2r_tot = sum([count for count in self.s2r])
        s2a_tot = sum([count for count in self.s2a])
        chi2, p, dof, ex = stats.chi2_contingency(np.array([[s1r_tot, s1a_tot],
                                                            [s2r_tot, s2a_tot]]))
        
        del s1r_tot, s1a_tot, s2r_tot, s2a_tot
        self.pvalue = p
        self.zscore = chi2
    
    def try_reads(self, **kwargs):
        #method = kwargs.get('switch_method', 'double')
        #sample = kwargs.get('sample', 's1')
        #if method == 'double':
        self.try_adjust_double()
        #else:
        #self.try_adjust_single('s1')
        self.chi_reads()
        
    def try_chi_count(self, **kwargs):
        self.try_adjust_double()
        self.chi_count()


class MemoTest(object):
        def __init__(self, window):
            self.pvalue = copy.deepcopy(window.pvalue)
            self.toswitch = copy.deepcopy(window.toswitch)

##########################################################################################

class RefList(list):  # inherited list attributes..to still use it as list but adding custom attributes
    
    def __init__(self):
        self.map = {}
        self.snps_dict = {}
        
    # simply create a dictionary where values are the position in the list    
    def mapme(self):
        for i in range(len(self)):
            self.map[self[i].chrom] = i

     
class RefObj(object):

    def __init__(self, chrom):
        self.chrom = chrom
        self.windows = []
        self.ranges = []
        self.phases = []

class Range(object):
    
    def __init__(self, SNPwin, overlap, minwin):

        self.SNPwin = SNPwin
        self.overlap = overlap
        self.minwin = minwin
        self.windows = []   ### everything stored in window objects

    @property
    def numSNP(self):
        return self.SNPwin + (self.SNPwin - self.overlap) * (len(self.windows) - 1)
    
    @property
    def s1AF(self):
        to_get = self.SNPwin - self.overlap
        s1AF = self.windows[0].sample1_AF       # initialize with whole first window array
        if len(self.windows) == 1:
            return s1AF
        for window in self.windows[1:]:
            s1AF += window.sample1_AF[-to_get:]
        return s1AF
        
    @property
    def s2AF(self):
        to_get = self.SNPwin - self.overlap
        s2AF = self.windows[0].sample2_AF       # initialize with whole first window array
        if len(self.windows) == 1:
            return s2AF
        for window in self.windows[1:]:
            s2AF += window.sample2_AF[-to_get:]
        return s2AF
        
    @property
    def cum_counts(self):
        to_get = self.SNPwin - self.overlap
        s1r = sum(self.windows[0].s1r_op)       # initialize with whole first window array
        s1a = sum(self.windows[0].s1a_op)
        s2r = sum(self.windows[0].s2r_op)
        s2a = sum(self.windows[0].s2a_op)
        if len(self.windows) == 1:
            return (s1r, s1a, s2r, s2a)
        for window in self.windows[1:]:
            s1r += sum(window.s1r_op[-to_get:])
            s1a += sum(window.s1a_op[-to_get:])
            s2r += sum(window.s2r_op[-to_get:])
            s2a += sum(window.s2a_op[-to_get:])
        return (s1r, s1a, s2r, s2a)
      
    @property
    def s1AFmed(self):
        return np.median(self.s1AF)
    
    @property
    def s2AFmed(self):
        return np.median(self.s2AF)
    
    @property
    def adjp_list(self):
        adjp_list = []
        for window in self.windows:
            adjp_list.append(window.adjp)
        return adjp_list

    @property
    def adjp_med(self):
        return np.median(self.adjp_list)
    
    @property
    def start(self):
        return self.windows[0].start
        
    @property
    def end(self):
        return self.windows[-1].end
    
    @property
    def winum(self):
        return len(self.windows)


class Hcphase(object):

    def __init__(self):
        self.chrom = False
        self.refs = []
        self.alts = []
        self.pattern = []
        self.pos = []

        
    def add(self, (switch, chrom, pos, ref, alt)):
        self.pattern.append(int(switch))
        if not self.chrom or self.chrom == chrom: 
            self.chrom = chrom
        else:
            logger.error("Reference changed within a phase at %s - %s" % (chrom, pos))
            raise
        self.pos.append(int(pos))
        self.refs.append(ref)
        self.alts.append(alt)
    
    @property
    def phased(self):
        return len(self.pos)
        
    @property
    def start(self):
        return self.pos[0]

    @property
    def end(self):
        return self.pos[-1]
