from distutils.core import setup

setup(
    name='xscan',
    version='0.1.1',
    description="Tool for discovery of genome mosaic structural variants using next generation sequencing",
    author='Davide Scaglione',
    author_email='dscaglione@igatechnology.com',
    license='GNU General Public License',
    url='http://bitbucket.org/dscaglione/xscan',
    packages=[
        'xscan',
    ],
    install_requires=[
        'numpy>=1.7.0'
        'scipy>=0.11.0'
        'vcf>=0.6.4',
        'matplotlib>=1.3.1',
		'multiprocessing>=0.70a1'
    ]
)