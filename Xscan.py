#!/usr/bin/env python
'''
MIT License

Copyright (c) 2016 Davide Scaglione (IGA - Istituito di Genomica Applicata)

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.


This script is the Python port of original allele frequency-based
detection of somatic deletion as designed by Fabio marroni.
Major implementations:
Benjamini-Hochberg correction for multiple testing
Chi-square and Fisher's method engines
Requires a multi-sample vcf file as minimal input.


Two different statistic engines validated:

#deprecated
- wmw_alt -: uses a Wilcoson Mann-Witney test on ALT allelic frequencies 
between the two samples (reference is expected to be mostly representative
of one of the two haplotypes, able to detect hemizygous deletions).

#deprecated
- wmw_try -: uses a Wilcoson Mann-Witney test on allelic frequencies 
between the two samples, where numerato is the allele giving the lowest
allelic frequency among the two samples (also works in samples where 
reference is not representative of one haplotype, able to detect
hemizygous deletions)

#deprecated
- chi_count -: Chi square test over observed frequency of SNP positions
having lower alternative allele frequencies in one sample against the other.
(reference is expected to be mostly representative of one of the two haplotypes).

----> chi_reads -: Chi square test over reads abundance of REF calls versus ALT calls.
(reference is expected to be mostly representative of one of the two haplotypes)

#deprecated
- chi_sum -: Per-position chi square contingency test over sample/allele reads 
numbers, yielding cumulative chi for each window to test over (also works in
samples where reference is not representative of one haplotype, BUT UNABLE 
to detect hemizygous deletions).

#deprecated
- meta_fisher -: Per-position Fisher's exact test within window, coupled with 
meta-analysis by Fisher's method to compute chi values over window (also works 
in samples where reference is not representative of one haplotype, able to 
detect hemizygous deletions).

----> phased_fisher: Fisher test on cumulative counts on small phased SNP blocks,
folowed by p-value meta analysis over the given window. It requires pahse blocks from HAPCUT 
software (also works in samples where reference is not representative of one haplotype, 
able to detect hemizygous deletions). 

Authors: Davide Scaglione, Fabio Marroni
Contact: dscaglione@igatechnology.com
Date of creation: 10-02-14
Version: 0.2.1

Changelog v0.2.1

- Chi_reads and try_reads methods implemented


Chengelog v0.2

- Implemented window analyisis
- Bug fix in chi_count method (excluded tie scenario from counting)
- dbSNP format dump, removed old SNP table output with window pvalue
- default adjusted pvalue 10e-3
- min heterozygosity for at least one sample set to default 0.30
- coeff to calculate max coverage from median changed to 2.5
- refinement of boundaries routine based on pvalue analysis (--refine)
- color coding of ranges fixed on the basis of absolute value of delta AF

<TAGS>
#SNP #VCF #somatic #deletions #chimeric #hemizygous #allelefrequency
</TAGS>


'''

#import vcf
import sys
import argparse
import os
import math
#import ipdb

import xscan.functions as Xscan
import xscan.phasing as Xphases

import logging

logging.basicConfig()
logger = logging.getLogger("Xscan")

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Detection of somatic' + \
                        'deletions by allele frequencies distortion',
                        #formatter_class=argparse.ArgumentDefaultsHelpFormatter
                        formatter_class=lambda prog: argparse.HelpFormatter(prog,
                                                                            max_help_position=20))
    parser.add_argument('--vcf', dest='vcf', required=True,
                        help='Input multi-sample vcf file, uncompressed; a bgzip indexed ' + \
                             'version will be generated automatically if not present')
    parser.add_argument('--min-sample-cov', dest='min_cov',
                        default=15, type=int,
                        help='Min per-sample SNP coverage (deafult: 15)')
    parser.add_argument('--cov-ratio', dest='covratio',
                        type=float, default=2.0,
                        help='Ratio to calculate maximum coverage over ' + \
                        'the estimated median coverage per sample (default: 2.0)')
    parser.add_argument('--min-one-het', dest='min_one_het',
                        type=float, default=0.30,
                        help='At least one sample should have this MAF to ' + \
                        'retain the position in the informative SNP sites ' + \
                        'cataloge (default: 0.30)')
    parser.add_argument('--min-wins', dest='minwin', type=int,
                        help='Minimum number of positive windows to' + \
                        'consider a range (default: {SNPwin / (SNPwin - overlap) + 1} i.e. ' + \
                        'two non-overlapping genomic segments)')
    parser.add_argument('--hapcut', dest='hapcut_file', required=False, default=False,
                        help='Provide phase info by hapcut output')
    parser.add_argument('--min-phased-collect', dest='min_phased', required=False,
                        help='Min contiguous phase information to consider while ' + \
                        'collectiong HAPCUT data (deafult: 10); this will defined the set of ' + \
                        'sampled SNPs for windows generation', default=10, type=int)
#     parser.add_argument('--min-block-size', dest='min_block_size', required=False,
#                         help='Min size of a block to consider in meta_fisher ' + \
#                         '(deafult: 10)', default=10, type=int)
    parser.add_argument('--blocks-thresholds', dest='block_thresholds', nargs=3,
                        default=[10,1,30],
                        help='min_block_size(SNPs) min_block_num(#) min_SNP_perwin(SNPs)' + \
                        '(deafult: 10 1 30)')
#     parser.add_argument('--blind-segmentation', dest='blind_segmentation', 
#                         nargs=2, type=int,
#                         default=[20, 20000], help="Parameters for blind window blocks' + \
#                         '(max-block-size(SNPs), max-block-span(bp)) default: [20, 20000]") 
    parser.add_argument('--sample1', dest='sample1', required=True,
                        help='Sample 1 name (as in vcf)')
    parser.add_argument('--sample2', dest='sample2', required=True,
                        help='Sample 2 name (as in vcf)')
    parser.add_argument('--out-folder', dest='out_folder', required=True,
                        help='Output folder')
    parser.add_argument('--SNPwin', dest='SNPwin', type=int, default=200,
                        help='Number of SNPs per window (deafult: 200)')
    parser.add_argument('--overlap', dest='overlap', type=int, default=180,
                        help='Number of overlapping SNPs between windows (deafult: 180)')
    parser.add_argument('--no-gapfill', dest='no_gap_filling',
                        action='store_true', default=False,
                        help='Skip gap filling routine')
    '''
    parser.add_argument('--permutations-cap', dest='perm_cap', required=False,
                        help='Cap for number of permutations in a window',
                        default = 1024, type=int)
    parser.add_argument('--override', dest='override',
                        action='store_true', default=False,
                        help='Override preset for permutation engine: SNPwin=100; overlap=30;' + \
                        'min-adjp=0.0001; min-phased-collect=3; min-phased-permute=3; min-wins=4')
    parser.add_argument('--min-phased-permute', dest='min_permute', required=False,
                        help='Min lenght of phase block to be subject of permutation',
                        default = 3, type=int)
    '''
    
#     parser.add_argument('--override', dest='override',
#                         action='store_true', default=False,
#                         help='Override preset for no phase information')
    parser.add_argument('--refine', dest='range_refinement',
                        action='store_true',
                        help='Perform refinement of ranges by inspection ' + \
                        'of pvalue slopes (default: disabled)')
    parser.add_argument('--max-gap', dest='maxgap', type=int,
                        help='Gapfill phase 1: Gap between positive ' + \
                        'windows will be filled if shorter than max-gap ' + \
                        '(default: floor(min-wins/2)')
    parser.add_argument('--gap-ratio', dest='gapratio', type=float,
                        default=0.50,
                        help='Gapfill phase 2: max ratio between gap and ' + \
                        'shorter positive range')
    parser.add_argument('--method', dest='method', type=str,
                        nargs='*', default=['chi_reads'],
                        choices=['chi_reads', 'phased_fisher'],
                        
                        # leftovers [ 'meta_fisher', 'chi_count', 'try_chi_count', 'try_reads'],
                        help='Anlysis methods\n' + \
                        'Set a space-separated list of analyisis method ' + \
                        'to perform\n')
    parser.add_argument('--select-refs', dest='contigs',
                        nargs='*', default=[],
                        help='Defined set of reference sequences to operate on (space separated)')
    parser.add_argument('--min-ref-length', dest='min_ref_length',
                        type=int, default=1000000,
                        help='Minimum length to process reference sequence; overrided by ' + \
                        '--select-refs (default: 1000000)')
    parser.add_argument('--min-adj-pvalue', dest='minadjp',
                        type=float, default=1e-3,
                        help='Minimum Benjamini-Hochberg adjusted pvalue' + \
                        ' to consider positive windows (deafault 1e-3)')
    parser.add_argument('--debug', dest='debug',
                        default=False, action='store_true',
                        help='Verbose printout for debug')
#     parser.add_argument('--threads', dest='threads',
#                         type=int, default=1,
#                         help='Threads for parallelization of window analyses')
#     
    my_args = parser.parse_args()
    
    if my_args.debug:
        logger.setLevel(logging.DEBUG)
    else:
        logger.setLevel(logging.INFO)
        
    #### important consistency check ############################################
    if my_args.overlap >= my_args.SNPwin or my_args.overlap < 0:
        sys.exit("Overlap needs to be >= 0 and lower than SNP window!\n")

    if (not ('phased_fisher' in my_args.method) and (my_args.hapcut_file)):
        sys.exit("You loaded phase information without invoking phased_fisher method\n")
    
    if ('phased_fisher' in my_args.method and not my_args.hapcut_file): 
#         if (my_args.blind_segmentation[0] < my_args.block_thresholds[0]):
#             sys.exit('Blind segmentation will generate blocks smaller {0} than the ' \
#                      'minimum required {1}\n'.format(my_args.blind_segmentation[0],
#                                                      my_args.block_thresholds[0]))
        sys.exit("HAPCUT data not provided to perform phased_fisher\n")
        
        
#         if not my_args.override:
# 
#             my_args.SNPwin = 50
#             my_args.overlap = 25
#             #my_args.gapratio = 0.75
#             my_args.minwin = 3
#             my_args.minadjp = 1e-3
#             my_args.block_thresholds = [4, 3, 15]
#             my_args.blind_segmentation = [15, 100000]
#             my_args.no_gap_filling = True
#              
#             logger.info("Since using no external phase information is provided parameters " + \
#                         "will be forced to:")
#             logger.info("SNPwin: %s" % (my_args.SNPwin))
#             logger.info("overlap: %s" %(my_args.overlap))
#             #print "gap-ratio: %s" %(my_args.gapratio)
#             logger.info("min-wins: %s" %(my_args.minwin))
#             logger.info("min-adjp: %s" %(my_args.minadjp))
#             logger.info("block-thresholds: %s" %(my_args.block_thresholds))
#             logger.info("blind-segmentation: %s" %(my_args.blind_segmentation))
#             logger.info("no-gap-filling: %s" %(my_args.no_gap_filling))
#             logger.info("Use --override to allow different values for these parameters")  

        
    #############################################################################
    
    if not os.path.exists(my_args.out_folder):
        os.makedirs(my_args.out_folder)
    
  
    #############################################################################
    
    sample1 = my_args.sample1
    sample2 = my_args.sample2
    vcf_file = my_args.vcf

    Xscan.tabix_me(vcf_file)
    
    bgzip_vcf = vcf_file + ".gz"
    
    ### Sampling coverage #######################################################
    (med_cov1, med_cov2) = Xscan.inspect_two_cov(bgzip_vcf, sample1, sample2)
    max_cov1 = int(med_cov1 * my_args.covratio)
    max_cov2 = int(med_cov2 * my_args.covratio)
    logger.info("Median coverage of " + sample1 + " is " + str(med_cov1))
    logger.info("Median coverage of " + sample2 + " is " + str(med_cov2))
    logger.info("will use maximum coverage of " + str(max_cov1) + " for " + sample1)
    logger.info("will use maximum coverage of " + str(max_cov2) + " for " + sample2)
    #############################################################################
    
    allowed_ref = Xscan.define_allowed_ref(bgzip_vcf,
                                           user_required=my_args.contigs,
                                           min_length=my_args.min_ref_length)
    
    
    
    ### Reading VCF first time to incorporate first dict of snp sites ###########
    
    ref_list = Xscan.scan_vcf(bgzip_vcf, sample1, sample2,
                        my_args.SNPwin,
                        my_args.overlap,
                        my_args.out_folder,
                        allowed_ref,
                        max_cov1=max_cov1,
                        max_cov2=max_cov2,
                        min_cov1=my_args.min_cov,
                        min_cov2=my_args.min_cov,
                        min_one_het=my_args.min_one_het,
                        dict_of_set=False)
    
    #############################################################################
    
    
    if my_args.hapcut_file:
        logger.info("Colleting phase information from HAPCUT and generating blocks")
        phase_list = Xphases.read_hapcut(my_args.hapcut_file, ref_list.snps_dict)
        #ipdb.set_trace()
        phase_list = Xphases.filter_phases(phase_list, min_phased=my_args.min_phased)
        #ipdb.set_trace()
        dict_of_phased = Xphases.generate_phase_set(phase_list) # for fast filtering lookup
        #vcf_reader = vcf.Reader(open(my_args.vcf, 'r'))
        
        #ipdb.set_trace()
        ref_list = Xscan.scan_vcf(bgzip_vcf, sample1, sample2,
                                  my_args.SNPwin,
                                  my_args.overlap,
                                  my_args.out_folder,
                                  allowed_ref,
                                  max_cov1=max_cov1,
                                  max_cov2=max_cov2,
                                  min_cov1=my_args.min_cov,
                                  min_cov2=my_args.min_cov,
                                  min_one_het=my_args.min_one_het,
                                  dict_of_set=dict_of_phased,
                                  using_phase_info=my_args.hapcut_file)
        # a new ref_list object is initiated, generating a 
        # dictionary of positions olny present in 
        # the phased dataset too
        # append phased blocks to relative chromosmes
        Xphases.phases_to_ref(phase_list, ref_list)
        # distribution routin to align phase blocks to SNPs windows
        #ipdb.set_trace()
        Xphases.distribute_blocks(ref_list)
        
#     else:
#         logger.info("HAPCUT information not provided, will use blind segmentation")
#         Xphases.blind_blocks(ref_list, my_args.blind_segmentation)

    



        
    #ipdb.set_trace()
    ### Apply methods and plot results ###
    for method in my_args.method:

        if my_args.minwin:
            minwin = my_args.minwin
        else: 
            minwin = math.ceil(my_args.SNPwin / (my_args.SNPwin -
                                                 my_args.overlap) + 1)
        logger.info("Min windows number to trigger range: " + str(int(minwin)))

        if my_args.maxgap:
            maxgap = my_args.maxgap
        else:
            maxgap = math.floor(minwin / 2)

        Xscan.run_analysis(ref_list, method, my_args.minadjp, maxgap,
                           minwin, my_args.out_folder, sample1, sample2,
                           my_args.SNPwin, my_args.overlap, my_args.gapratio,
                           no_gap_filling=my_args.no_gap_filling,
                           range_refinement=my_args.range_refinement,
                           threads=1,
                           #min_to_permute=my_args.min_permute,
                           #permutations_cap=my_args.perm_cap,
                           block_thresholds=my_args.block_thresholds)


    #ipdb.set_trace()
