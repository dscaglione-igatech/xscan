#!/usr/bin/env sh




display_usage() { 
	echo -e "\nUtility script for HAPCUT haplotype reconstruction" 
	echo -e "Usage:\n$0 <sorted_bamfile> <reference_fasta> <output_Prefix> \n" 
	} 



if [  $# != 3 ] 
	then 
		display_usage
		exit 1
	fi 
 
 
	if [[ ( $# == "--help") ||  $# == "-h" ]] 
	then 
		display_usage
		exit 0
	fi





base=$(dirname $0)/ext_bin

bamfile=$1 #/projects/vitis/lanes/alignments/BWA/dna/pinot_noir_VCR18/pinot_noir_VCR18_uniq_realigned.bam
reference=$2 #/genomes/vitis_vinifera/assembly/reference/12xCHR/vitis_vinifera_12xCHR.fasta
prefix=$3 #prova_pinot_noir_VCR18

samtools_path=${base}/samtools
vcfutils_script=${base}/vcfutils.pl
bcftools_path=${base}/bcftools
hairs_path=${base}/extractHAIRS
hapcut_path=${base}/HAPCUT



${samtools_path} mpileup -q 20 -guDSs -f ${reference} ${bamfile} | ${bcftools_path} view -Nvcg - | ${vcfutils_script} varFilter -d 10 -Q 30 -w 20 > ${prefix}.vcf

${hairs_path} --VCF ${prefix}.vcf --bam ${bamfile} --indels 1 --ref ${reference} --mmq 25 > ${prefix}_informative_reads.txt

${hapcut_path} --VCF ${prefix}.vcf --fragments ${prefix}_informative_reads.txt --output ${prefix}_phased.hapcut




